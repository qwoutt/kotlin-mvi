package course.search.learn.contragents.model

import course.search.learn.contragents.domain.database.entities.Contragent
import java.io.Serializable


data class ContragentsList(val Contragents: List<Contragent>){
}
