package course.search.learn.contragents.search.presenter
import android.util.Log
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.domain.network.DaDataApiServiсe
import course.search.learn.contragents.interactors.DBInteractor
import course.search.learn.contragents.interactors.NetworkInteracor
import course.search.learn.contragents.search.ContragentView
import course.search.learn.contragents.search.SearchViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class SearchPresenter(api: DaDataApiServiсe, db : AppDatabase): MviBasePresenter<ContragentView, SearchViewState>() {

    private val dbInteractor = DBInteractor(db)
    private val interactor = NetworkInteracor(api)
    private lateinit var observer : Observable<SearchViewState>

    public override fun bindIntents() {

        Log.i("intent", "bindIntents")
        observer = intent(ContragentView::searchIntent)
                .doOnNext{s -> Log.i("next", s)}
                .switchMap(interactor::search)
                .observeOn(AndroidSchedulers.mainThread())

        dbInteractor.subscribeOnAdd(intent(ContragentView::saveIntent))

        subscribeViewState(observer, ContragentView::render)

    }

}
