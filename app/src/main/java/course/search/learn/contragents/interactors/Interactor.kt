package course.search.learn.contragents.interactors

import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.search.SearchViewState
import io.reactivex.functions.Function


abstract class Interactor {

    fun stateMapper(query: String) = Function<List<Contragent>, SearchViewState> { list ->
            if(list.isEmpty()) SearchViewState.Empty(query)
            else  SearchViewState.Complite(query, list)
    }


}