package course.search.learn.contragents.search.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.SearchView
import com.hannesdorfmann.mosby3.mvi.MviFragment
import com.hannesdorfmann.mosby3.mvi.MviPresenter
import com.jakewharton.rxbinding2.widget.RxSearchView
import course.search.learn.contragents.R
import course.search.learn.contragents.details.activity.DetailsActivity
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.search.ContragentView
import course.search.learn.contragents.search.SearchViewState
import course.search.learn.contragents.search.adapter.ContragentAdapter
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.include_error_view.*
import java.util.concurrent.TimeUnit

abstract class BaseFragment <T : MviPresenter<ContragentView, *>?> : MviFragment<ContragentView,T>(), ContragentView {

    lateinit var searchView: SearchView
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: ContragentAdapter

    private var searchObserver = PublishSubject.create<String>()
    protected var saveActionObserver = PublishSubject.create<Contragent>()

    private val search_fragment_lifacycle_tag = "CALL"

    protected var action = {
        agent: Contragent ->
        renderLoad()
        saveActionObserver.onNext(agent)
        DetailsActivity.start(activity, agent)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.i(search_fragment_lifacycle_tag, "on create view")
        return inflater?.inflate(R.layout.fragment_search, container, false)?.let {
            setHasOptionsMenu(true)

            recyclerView  = it.findViewById(R.id.recycler_view)
            adapter = ContragentAdapter(action)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter

            it
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        Log.i(search_fragment_lifacycle_tag, "on menu create")
        inflater?.inflate(R.menu.menu_search, menu)
        searchView = menu!!.findItem(R.id.search_view).actionView as SearchView
        createSearchDisposable()
    }

    override fun searchIntent(): Observable<String> {
        return searchObserver
    }

    override fun saveIntent(): Observable<Contragent> {
        return saveActionObserver
    }

    private fun createSearchDisposable(): Disposable? {
        return RxSearchView.queryTextChanges(searchView)
                .map { it.toString().trim() }
                .filter{ str -> str.length > 2 || str.isEmpty()  }
                .debounce(900, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribe(searchObserver::onNext)
    }

    override fun render(state: SearchViewState) {
        when(state){

            is SearchViewState.Pending -> {
                recyclerView.visibility = View.GONE
                progessBar.visibility = View.GONE
                errorView.visibility = View.GONE
                emptyView.visibility = View.GONE
            }

            is SearchViewState.Error -> {
                recyclerView.visibility = View.GONE
                progessBar.visibility = View.GONE
                errorView.visibility = View.VISIBLE
                emptyView.visibility = View.GONE
            }

            is SearchViewState.Empty -> {
                recyclerView.visibility = View.GONE
                progessBar.visibility = View.GONE
                errorView.visibility = View.GONE
                emptyView.visibility = View.VISIBLE
            }

            is SearchViewState.Loading -> {
                renderLoad()
            }

            is SearchViewState.Complite -> {
                recyclerView.visibility = View.VISIBLE
                progessBar.visibility = View.GONE
                errorView.visibility = View.GONE
                emptyView.visibility = View.GONE
                adapter.setData(state.contragents)
            }
        }
    }

    private fun renderLoad(){
        recyclerView.visibility = View.GONE
        progessBar.visibility = View.VISIBLE
        errorView.visibility = View.GONE
        emptyView.visibility = View.GONE
    }
}