package course.search.learn.contragents.interactors

import course.search.learn.contragents.domain.network.DaDataApiServiсe
import course.search.learn.contragents.domain.network.Query

import course.search.learn.contragents.search.SearchViewState
import course.search.learn.contragents.utils.isClearInput
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


open class NetworkInteracor(private val api : DaDataApiServiсe) : Interactor() {

    public open fun search(query : String) : Observable<SearchViewState>{
        if(query.isClearInput()){
             return Observable.just(SearchViewState.Pending())
        }

        return api.searchContragents(Query(query))
                .map { stateMapper(query).apply(it.Contragents) }
                .startWith(SearchViewState.Loading())
                .onErrorReturn {
                    error -> error.printStackTrace()
                    SearchViewState.Error(query,error)}
                .toObservable()
    }

}