package course.search.learn.contragents.search.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import course.search.learn.contragents.R
import course.search.learn.contragents.domain.database.entities.Contragent
import kotlinx.android.synthetic.main.item_contragent.view.*
import java.util.*


class ContragentAdapter( val action : (Contragent) -> Unit) : RecyclerView.Adapter<ContragentAdapter.ViewHolder>() {

    private var items : List<Contragent> = Collections.emptyList()

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        return ViewHolder(parent.inflate(R.layout.item_contragent))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], action)
    }

    override fun getItemCount() = items.size

    public fun setData(items: List<Contragent>){
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(item: Contragent, action: (Contragent) -> Unit) {
            itemView.apply {
                title_text.text = item.name
                description_text.text = item.manager_name
                address_text.text = item.address
            }
            RxView.clicks(itemView).subscribe{ action.invoke(item) }
        }
    }

}