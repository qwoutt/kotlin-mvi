package course.search.learn.contragents.search.fragment
import course.search.learn.contragents.details.activity.DetailsActivity
import course.search.learn.contragents.domain.AppDelegate
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.search.presenter.SearchPresenter

open class SearchFragment : BaseFragment<SearchPresenter>(){
    init {
        action = {
            agent: Contragent ->
            saveActionObserver.onNext(agent)
            DetailsActivity.start(activity,agent) }
    }


    override fun createPresenter()= AppDelegate.from(activity).run {
        SearchPresenter(api, db)
    }

}