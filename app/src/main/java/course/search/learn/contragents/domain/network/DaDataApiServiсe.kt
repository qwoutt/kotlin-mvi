package course.search.learn.contragents.domain.network

import course.search.learn.contragents.BuildConfig
import course.search.learn.contragents.model.ContragentsList
import io.reactivex.Flowable

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface DaDataApiServiсe {
    @POST("suggestions/api/4_1/rs/suggest/party")
    @Headers("Authorization: Token ${BuildConfig.API_KEY}")
    fun searchContragents(@Body body: Query) : Flowable<ContragentsList>
}

data class Query(val query  : String)