package course.search.learn.contragents.domain.database.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import course.search.learn.contragents.BuildConfig;
import course.search.learn.contragents.domain.database.entities.Contragent;

@Dao
public abstract class ContragentDao {

    @Query("SELECT * FROM CONTRAGENT ")
    public abstract List<Contragent> getAll();

    @Query("SELECT * FROM " + "CONTRAGENT" + " WHERE ID == :ID")
    public abstract Contragent get(Long ID);

    @Query("SELECT * FROM " + "CONTRAGENT" + " WHERE inn == :inn")
    public abstract Contragent getByInn(Long inn);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public abstract Long insert(Contragent question);

    @Delete
    public abstract void delete(Contragent question);

    @Query("DELETE FROM " + "CONTRAGENT")
    public abstract void deleteAll();

    @Query("DELETE FROM " + "CONTRAGENT" + " WHERE ID = :ID")
    public abstract void delete(Long ID);

}
