package course.search.learn.contragents.search.fragment
import course.search.learn.contragents.details.activity.DetailsActivity
import course.search.learn.contragents.domain.AppDelegate
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.search.presenter.SavedPresenter

class SavedFragment : BaseFragment<SavedPresenter>() {

    init {
        action = {
            agent: Contragent ->
            DetailsActivity.start(activity,agent) }
    }

    override fun createPresenter() = AppDelegate.from(activity).run {
        SavedPresenter(db)
    }
}