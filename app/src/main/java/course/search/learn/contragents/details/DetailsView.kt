package course.search.learn.contragents.details

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Consumer


interface DetailsView : MvpView {
    fun render(state: CommonState)
    fun buttonIntent() : Observable<Any>
    fun checkFavoritIntent() : Observable<Any>
}