package course.search.learn.contragents.domain.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;


import course.search.learn.contragents.domain.database.entities.Contragent;
import course.search.learn.contragents.domain.database.entities.Favorit;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;


@Dao
public abstract class FavoritDao {

    @Transaction
    @Query("SELECT * FROM Favorit , Contragent " +
            " WHERE Favorit.contragentID = Contragent.ID")
    public abstract List<Contragent> getAll();

    @Query("SELECT * FROM " + "Favorit" + " WHERE id == :id")
    public abstract Flowable<Favorit> get(Long id);

    @Query("SELECT * FROM favorit, contragent " +
            "WHERE contragent.inn == :inn AND contragent.ID = favorit.contragentID")
    public abstract Favorit getFavoritContragentByInn(Long inn);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public abstract void insert(Favorit favorit);

    @Delete
    public abstract void delete(Favorit favorit);

    @Query("DELETE FROM " + "CONTRAGENT")
    public abstract void deleteAll();

    @Query("SELECT * FROM favorit, contragent WHERE contragent.id = favorit.contragentID")
    public abstract List<Favorit> getAllForTest();
}
