package course.search.learn.contragents.domain.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import course.search.learn.contragents.domain.database.dao.CacheDao
import course.search.learn.contragents.domain.database.dao.ContragentDao
import course.search.learn.contragents.domain.database.dao.FavoritDao
import course.search.learn.contragents.domain.database.entities.Cache
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.domain.database.entities.Favorit


@Database(entities = arrayOf(Contragent::class, Favorit::class, Cache::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    public abstract fun contragentDao(): ContragentDao
    public abstract fun cacheDao(): CacheDao
    public abstract fun favoritDao(): FavoritDao

}