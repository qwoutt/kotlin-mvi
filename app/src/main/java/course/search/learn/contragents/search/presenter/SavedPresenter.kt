package course.search.learn.contragents.search.presenter

import android.util.Log
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.interactors.DBInteractor
import course.search.learn.contragents.search.ContragentView
import course.search.learn.contragents.search.SearchViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers


class SavedPresenter(db : AppDatabase) : MviBasePresenter<ContragentView, SearchViewState>() {

    private val cacheInteractor = DBInteractor(db)

    private lateinit var observer : Observable<SearchViewState>
    private lateinit var cacheobserver: Observable<Contragent>

    public override fun bindIntents() {

        observer = intent(ContragentView::searchIntent)
                .doOnNext{s -> Log.i("next", s)}
                .switchMap(cacheInteractor::search)
                .observeOn(AndroidSchedulers.mainThread())

        cacheobserver = intent(ContragentView::saveIntent)
        cacheInteractor.subscribeOnAdd(cacheobserver)

        subscribeViewState(observer, ContragentView::render)
    }

}