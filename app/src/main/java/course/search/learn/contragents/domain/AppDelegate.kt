package course.search.learn.contragents.domain

import android.app.Application
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.network.DaDataApiServiсe
import course.search.learn.contragents.domain.network.OkHttpBuilder
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class AppDelegate : Application() {

    private val URL: String = "https://suggestions.dadata.ru/"
    lateinit var api: DaDataApiServiсe
    lateinit var db : AppDatabase

    fun clear(){
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .subscribe { d -> d.contragentDao().deleteAll() }
    }

    override fun onCreate() {
        super.onCreate()
        api = createApiService(OkHttpBuilder.create(this))
        db  = createDB(this)
        clear()
    }

    public fun createApiService(client: OkHttpClient?) : DaDataApiServiсe {
        return Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(OkHttpBuilder.createGsonFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .let{
                    if(client != null)  it.client(client)
                    it
                }
                .build()
                .create(DaDataApiServiсe::class.java)
    }

    public fun createDB(context: Context) : AppDatabase{
        return Room.databaseBuilder(context,
                AppDatabase::class.java,
                "ContragentDB")
                .fallbackToDestructiveMigration()
                .build()
    }

    companion object {
        fun from(context: Context) : AppDelegate{
            return context.applicationContext as AppDelegate
        }
    }

}