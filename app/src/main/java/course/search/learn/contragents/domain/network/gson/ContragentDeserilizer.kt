package course.search.learn.contragents.domain.network.gson

import com.google.gson.*
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.model.ContragentsList
import java.lang.reflect.Type


class ContragentDeserilizer : JsonDeserializer<ContragentsList> {

    private fun deserializeItem(jsonObject: JsonObject): Contragent{
        val sugg_data  = jsonObject.getAsJsonObject("data")
        val contragent = Contragent()

        contragent.name = jsonObject.get("value").asString
        contragent.kpp  = sugg_data?.get("kpp").let { if( it is JsonNull ) 0 else it?.asLong}
        contragent.inn  = sugg_data?.get("inn").let { if( it is JsonNull ) 0 else it?.asLong}
        contragent.ogrn = sugg_data?.get("ogrn").let { if( it is JsonNull ) 0 else it?.asLong}

        sugg_data?.get("management")?.let {
            if(it is JsonObject){
                contragent.manager_name = it.get("name")?.asString
                contragent.manager_post = it.get("post")?.asString
            }
        }

        val address =  sugg_data.getAsJsonObject("address")
        contragent.address = address?.get("unrestricted_value")?.asString

        address?.get("data")?.let{
            if(it is JsonObject){
                contragent.lat = it.get("geo_lat")?.asDouble
                contragent.lon = it.get("geo_lon")?.asDouble
            }
        }
        return contragent
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ContragentsList {
        println(json.toString())
        val jsonObj = json!!.asJsonObject
        var jsonArr = jsonObj.getAsJsonArray("suggestions")
        val list = ArrayList<Contragent>()
        jsonArr.forEach { j -> list.add(deserializeItem(j.asJsonObject)) }
        return ContragentsList(list)
    }

}