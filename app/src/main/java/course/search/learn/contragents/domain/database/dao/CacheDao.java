package course.search.learn.contragents.domain.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import course.search.learn.contragents.domain.database.entities.Cache;
import course.search.learn.contragents.domain.database.entities.Contragent;
import course.search.learn.contragents.domain.database.entities.Favorit;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public abstract class CacheDao {

    @Query("SELECT * FROM " + "Cache" + " WHERE ID == :id")
    public abstract Single<Cache> get(Long id);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public abstract void insert(Cache cache);

    @Delete
    public abstract void delete(Cache cache);

    @Query("DELETE FROM " + "Cache")
    public abstract void deleteAll();

    @Transaction
    @Query("SELECT * FROM Cache , Contragent " +
            " WHERE Cache.contragentID = Contragent.ID" +
            " ORDER BY Cache.createTime")
    public abstract List<Contragent> getAll();

    @Query("DELETE FROM " + "Cache" + " WHERE ID = :ID")
    public abstract void delete(Long ID);


    @Query("SELECT * FROM Contragent ")
    public abstract Flowable<List<Contragent>> getTest();


    @Query("SELECT * FROM cache, contragent WHERE contragent.id = cache.contragentID")
    public abstract List<Cache> getAllForTest();
}
