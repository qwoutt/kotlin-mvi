package course.search.learn.contragents.details


interface DetailViewState {

    interface MapState{
        class HaveMap : MapState
        class NotHaveMap : MapState
    }

    interface ButtonState{
        class IsFavorit : ButtonState
        class IsNotFavorit : ButtonState
    }

}


class CommonState(val buttonState: DetailViewState.ButtonState,
                  val mapState:    DetailViewState.MapState)

