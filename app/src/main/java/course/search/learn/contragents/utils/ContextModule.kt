package course.search.learn.contragents.utils

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private var context : Context) {
    @Provides
    fun context() = context.applicationContext
}