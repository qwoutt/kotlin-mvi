package course.search.learn.contragents.search.activity

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import course.search.learn.contragents.R
import course.search.learn.contragents.search.fragment.SearchFragment
import android.support.design.widget.TabLayout
import course.search.learn.contragents.search.adapter.ViewPagerAdapter
import course.search.learn.contragents.search.fragment.SavedFragment
import kotlinx.android.synthetic.main.activity_main.*


class SearchActivity : AppCompatActivity() {


    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        fun initViewPager(){

            ViewPagerAdapter(supportFragmentManager).apply {
                add(Pair(SearchFragment(), getString(R.string.tab_search)))
                add(Pair(SavedFragment(), getString(R.string.tab_saved)))
            }.also {
                container.adapter = it
            }

            tabs.setupWithViewPager(container)

            container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
            tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewPager()
    }

}
