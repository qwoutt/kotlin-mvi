package course.search.learn.contragents.interactors

import android.annotation.SuppressLint
import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.domain.database.entities.Favorit
import io.reactivex.Single
import timber.log.Timber


class TransactionInteractor(db: AppDatabase) {

    private val cacheDao   = db.cacheDao()
    private val agentDao   = db.contragentDao()
    private val favoritDao = db.favoritDao()

    @SuppressLint("TimberArgCount")
    public fun addToFavorits(contragent: Contragent){

        cacheDao.allForTest.forEach { c -> Timber.d("cache", c.toString() ) }
        favoritDao.allForTest.forEach { c -> Timber.d("favorit", c.toString() ) }

        var agent =  agentDao.getByInn(contragent.inn)
        if(agent != null ) {
            cacheDao.delete(agent.ID)
            favoritDao.insert(Favorit(agent.ID))
        }
    }

    public fun RemoveFromFavorits(contragent: Contragent){
        agentDao.delete(contragent)// использую каскадную опцию удаления из рума
    }

    public fun isFavorit(contragent: Contragent): Boolean {
        return favoritDao.getFavoritContragentByInn(contragent.inn) != null
    }
}