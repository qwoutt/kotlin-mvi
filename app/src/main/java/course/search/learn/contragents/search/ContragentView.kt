package course.search.learn.contragents.search

import com.hannesdorfmann.mosby3.mvp.MvpView
import course.search.learn.contragents.domain.database.entities.Contragent
import io.reactivex.Observable


interface ContragentView : MvpView {
    fun render(state: SearchViewState)
    fun searchIntent(): Observable<String>
    fun saveIntent(): Observable<Contragent>
}