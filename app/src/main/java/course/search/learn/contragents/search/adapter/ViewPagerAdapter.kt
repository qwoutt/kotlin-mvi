package course.search.learn.contragents.search.adapter
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import course.search.learn.contragents.search.fragment.BaseFragment
import course.search.learn.contragents.search.fragment.SearchFragment


class ViewPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private var fragments = ArrayList<BaseFragment<*>>(2)
    private var titles    = ArrayList<String>(2)

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    public fun add(pairs : Pair<BaseFragment<*>,String>){
       pairs.apply {
           fragments.add(first)
           titles.add(second)
       }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position]
    }

}