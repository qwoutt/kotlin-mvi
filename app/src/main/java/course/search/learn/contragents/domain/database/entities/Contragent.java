package course.search.learn.contragents.domain.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import java.io.Serializable;


@Entity
public class Contragent implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public long ID;
    public String name;
    @Nullable
    public Long kpp;
    public String manager_name;
    public String manager_post;
    @Nullable
    public Long inn;
    @Nullable
    public Long ogrn;
    public String address;
    public Double lat;
    public Double lon;

    public Contragent(){}

    @Override
    public String toString() {
        return "Contragent{" +
                "name='" + name + '\'' +
                ", kpp=" + kpp +
                ", inn=" + inn +
                ", ogrn=" + ogrn +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contragent that = (Contragent) o;

        if (ID != that.ID) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (kpp != null ? !kpp.equals(that.kpp) : that.kpp != null) return false;
        if (inn != null ? !inn.equals(that.inn) : that.inn != null) return false;
        return ogrn != null ? ogrn.equals(that.ogrn) : that.ogrn == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (ID ^ (ID >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (kpp != null ? kpp.hashCode() : 0);
        result = 31 * result + (inn != null ? inn.hashCode() : 0);
        result = 31 * result + (ogrn != null ? ogrn.hashCode() : 0);
        return result;
    }
}
