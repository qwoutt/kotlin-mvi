package course.search.learn.contragents.domain.database.entities;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "favorit",
        foreignKeys ={
                @ForeignKey(
                        entity = Contragent.class,
                        parentColumns = "ID",
                        childColumns = "contragentID",
                        onUpdate = CASCADE,
                        onDelete = CASCADE
                )}
)
public class Favorit {
    @PrimaryKey(autoGenerate = true)
    public long ID;
    public long contragentID;


    public Favorit(long contragentID) {
        this.contragentID = contragentID;
    }
}
