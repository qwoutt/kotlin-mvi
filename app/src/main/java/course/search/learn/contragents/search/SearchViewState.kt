package course.search.learn.contragents.search

import course.search.learn.contragents.domain.database.entities.Contragent

interface SearchViewState {
    class Loading : SearchViewState
    class Empty(val query : String) : SearchViewState
    class Complite(val query: String,
                   val contragents : List<Contragent>) : SearchViewState
    class Error(val query: String,
                val error : Throwable) : SearchViewState
    class Pending : SearchViewState
}