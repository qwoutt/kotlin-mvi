package course.search.learn.contragents.details.activity


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.ShareActionProvider
import android.view.Menu
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding2.view.RxView

import course.search.learn.contragents.R
import course.search.learn.contragents.details.CommonState
import course.search.learn.contragents.details.DetailViewState
import course.search.learn.contragents.details.DetailsView
import course.search.learn.contragents.details.presenter.DetailsPresenter
import course.search.learn.contragents.domain.AppDelegate
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.utils.renderIfContentNotEmpty
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_details.*

open class DetailsActivity : MviActivity<DetailsView, DetailsPresenter>(), DetailsView {

    private lateinit var latlang: LatLng
    private lateinit var contragent: Contragent
    private lateinit var mShareActionProvider: ShareActionProvider
    private var gmap: GoogleMap? = null
    private val zoom = 12f
    private var mapView: MapView? = null
    private var buttonObserver : Observable<Any>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contragent = intent.getSerializableExtra(CONTRAGENT_KEY) as Contragent
        setContentView(R.layout.activity_details)
        bindContent(contragent)
        setSupportActionBar(toolbar)
        parseLatLng(contragent)
        createMap(savedInstanceState)
    }


    override fun createPresenter(): DetailsPresenter {
        return DetailsPresenter(contragent, AppDelegate.from(this).db)
    }

    private fun parseLatLng(contragent: Contragent?){
        latlang = LatLng(
                contragent?.lat?.toDouble() ?: 0.0,
                contragent?.lon?.toDouble() ?: 0.0)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_details, menu)
        var item = menu?.findItem(R.id.menu_item_share)
        mShareActionProvider = MenuItemCompat.getActionProvider(item) as ShareActionProvider
        setShareIntent()
        return true
    }

    private fun setShareIntent() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, contragent.toShare())
        sendIntent.type = "text/plain"
        mShareActionProvider.setShareIntent(sendIntent)
    }

    private fun bindContent(contragent: Contragent?) {
        toolbar.title = ""
        title_content.text = contragent?.name
        renderIfContentNotEmpty(kpp,  contragent?.kpp)
        renderIfContentNotEmpty(inn,  contragent?.inn)
        renderIfContentNotEmpty(ogrn, contragent?.ogrn)
        renderIfContentNotEmpty(name, contragent?.manager_name)
        renderIfContentNotEmpty(post, contragent?.manager_post)
        renderIfContentNotEmpty(address, contragent?.address)
    }

    //todo
    private fun createMap(savedInstanceState: Bundle?){
        if(latlang != LatLng(0.0,0.0)) {
            mapView = findViewById(R.id.mapView)
            mapView?.onCreate(savedInstanceState)
            mapView?.getMapAsync { map ->
                run {
                    gmap = map
                    map.setMinZoomPreference(zoom)
                    map.moveCamera(
                            CameraUpdateFactory.newLatLng(latlang)
                    )

                    map.addMarker(
                            MarkerOptions()
                                    .position(latlang)
                                    .title(contragent.name)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                                    .draggable(false).visible(true)
                    )
                }
            }
        }
    }

    companion object {
        public val CONTRAGENT_KEY = "CONTRAGENT_KEY"

        public fun start(activity: Activity, agent: Contragent) {
            activity.startActivity(
                    Intent(activity, DetailsActivity::class.java)
                            .putExtra(CONTRAGENT_KEY, agent)
            )
        }

    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    fun Contragent.toShare():String{
        return  "$name         \n" +
                "ИНН   : $inn  \n" +
                "КПП   : $kpp  \n" +
                "ОГРН  : $ogrn \n" +
                "адрес : $address \n"
    }


    override fun render(state: CommonState) {

        when(state.buttonState){

            is DetailViewState.ButtonState.IsFavorit -> {
                fab.setImageDrawable(
                        getDrawable(android.R.drawable.ic_delete))
            }

            else -> {
                fab.setImageDrawable(
                        getDrawable(android.R.drawable.ic_menu_add))
            }
        }

        when(state.mapState){
            is DetailViewState.MapState.HaveMap-> mapView?.visibility = View.VISIBLE
            else -> mapView?.visibility = View.GONE
        }
    }

    override fun buttonIntent(): Observable<Any> {
        return createFabEvent()
    }

    fun createFabEvent(): Observable<Any> {
       return RxView.clicks(fab)
    }

    override fun checkFavoritIntent(): Observable<Any> {
        return Observable.just(Any())
                // checkState)
    }


}

