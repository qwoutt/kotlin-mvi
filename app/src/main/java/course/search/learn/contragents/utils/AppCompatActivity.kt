package course.search.learn.contragents.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity


/**
 * The `fragment` is added to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.replaceFragmentInActivity(fragment: Fragment, @IdRes frameId: Int) {
    supportFragmentManager.transact {
        replace(frameId, fragment)
    }
}

/**
 * The `fragment` is added to the container view with tag. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.addFragmentToActivity(fragment: Fragment, tag: String) {
    supportFragmentManager.transact {
        add(fragment, tag)
    }
}

fun AppCompatActivity.setupActionBar(@IdRes toolbarId: Int, action: ActionBar.() -> Unit) {
    setSupportActionBar(findViewById(toolbarId))
    supportActionBar?.run {
        action()
    }
}

/**
 * Runs a FragmentTransaction, then calls commit().
 */
private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

public fun renderIfContentNotEmpty(view: TextView, content: String?){
    if(content != null && content .isNotEmpty()){
        view.text =  content
    }else {
        view.text =  ""
    }
}

public fun renderIfContentNotEmpty(view: TextView, content: Long?){
    if(content != null && content > 0){
        view.text =  content.toString()
    }else {
        view.text =  ""
    }
}



public fun renderIfContentNotEmpty(view: View, content: Any?){
    if(content == null){
        view.visibility = View.GONE
    }
}

public fun internetEnable(context: Context): Boolean {
    val cm  = context.getSystemService(Context.CONNECTIVITY_SERVICE)
            as ConnectivityManager
    val net = cm.activeNetworkInfo
    return (net != null && net.isConnected)
}


public fun String.isClearInput() : Boolean{
   return (this.isEmpty() || this.replace(" ","").isEmpty())
}
