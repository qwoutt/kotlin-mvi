package course.search.learn.contragents.details.presenter

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import course.search.learn.contragents.details.CommonState
import course.search.learn.contragents.details.DetailViewState

import course.search.learn.contragents.details.DetailsView
import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.interactors.TransactionInteractor
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class DetailsPresenter() : MviBasePresenter<DetailsView,CommonState>() {

    constructor(agent: Contragent, db : AppDatabase) : this() {
        contragnet = agent
        interactor = TransactionInteractor(db)
    }

    lateinit var contragnet : Contragent
    private var interactor : TransactionInteractor? = null

    private fun transactionStrategy(isFavorit: Boolean): Boolean{
        if(isFavorit){
            interactor?.RemoveFromFavorits(contragnet)
        }else{
            interactor?.addToFavorits(contragnet)
        }
        return !isFavorit
    }

    fun stateButtonStrategy(isFavorit: Boolean):  DetailViewState.ButtonState{
        return when(isFavorit){
            true  -> DetailViewState.ButtonState.IsFavorit()
            else  -> DetailViewState.ButtonState.IsNotFavorit()
        }
    }

    fun stateMapStrategy(): DetailViewState.MapState{
        val bool = contragnet.lat != null && contragnet.lon != null
        return when(bool){
            true  -> DetailViewState.MapState.HaveMap()
            false -> DetailViewState.MapState.NotHaveMap()
        }
    }

    override fun bindIntents() {
        val clickObservable = intent(DetailsView::buttonIntent)
                .observeOn(Schedulers.io())
                .map{ (interactor!!.isFavorit(contragnet))
                        .let { val fromTransactions = transactionStrategy(it)
                            stateButtonStrategy(fromTransactions)
                        }
                }
        val checkFavoritSingle = intent(DetailsView::checkFavoritIntent)
                .observeOn(Schedulers.io())
                .map{ interactor!!.isFavorit(contragnet)
                        .let { stateButtonStrategy(it) }
                }
        val statusState = Observable.merge(clickObservable,checkFavoritSingle)
        val mapStateObserver = Observable.just(stateMapStrategy())
        val zipper = BiFunction(::CommonState)
        val observer = Observable.combineLatest(statusState, mapStateObserver, zipper).observeOn(AndroidSchedulers.mainThread())

        subscribeViewState(observer, DetailsView::render)
    }
}