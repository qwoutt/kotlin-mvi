package course.search.learn.contragents.domain.network

import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.domain.network.gson.ContragentDeserilizer
import course.search.learn.contragents.model.ContragentsList
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


class OkHttpBuilder {
    val CACHE_SIZE  = 10 * 1000 * 1000L
    val CACHE_AGE   = 60 * 60 * 24
    val CACHE_STALE = 60 * 60 * 24 * 28

    val logger = HttpLoggingInterceptor(HttpLoggingInterceptor
            .Logger { message -> Log.d("http", message) })

    fun okHttpClient(): OkHttpClient {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(logger)
                .build()
    }


    fun okHttpClient(context: Context): OkHttpClient {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.HEADERS))
                .addNetworkInterceptor(OnlineInterceptor(context))
                .cache(initCache(file(context)))
                .build()
    }


    fun initCache(cacheFile: File): Cache {
        return Cache(cacheFile, CACHE_SIZE)
    }

    fun file(context: Context): File {
        val file = File(context.cacheDir, "HttpCache")
        if(!file.exists())
            file.mkdirs()
        return file
    }

    companion object {
        fun create(context: Context): OkHttpClient {
            return OkHttpBuilder().okHttpClient(context)
        }

        public fun createGsonFactory(): GsonConverterFactory? {
            return GsonConverterFactory.create(
                    GsonBuilder()
                            .registerTypeAdapter(ContragentsList::class.java, ContragentDeserilizer())
                            .create()
            )
        }
    }

    inner class OnlineInterceptor(var context: Context) : Interceptor{
        override fun intercept(chain: Interceptor.Chain?): Response {
            val cache =  CacheControl.Builder()
                    .maxAge(CACHE_AGE, TimeUnit.SECONDS)
                    .build()
            return chain!!.proceed(chain.request())
                    .newBuilder()
                    .header("Cache-Control", cache.toString())
                    .build()
        }
    }
}