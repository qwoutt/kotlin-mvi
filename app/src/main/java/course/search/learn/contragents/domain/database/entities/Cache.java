package course.search.learn.contragents.domain.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "Cache",
        foreignKeys ={
                @ForeignKey(
                        entity = Contragent.class,
                        parentColumns = "ID",
                        childColumns = "contragentID",
                        onUpdate = CASCADE,
                        onDelete = CASCADE
                )}
)
public class Cache {
    @PrimaryKey(autoGenerate = true)
    public long ID;
    public long contragentID;
    public long createTime;
    public long deleteTime;


    public Cache(long contragentID, long createTime, long deleteTime) {
        this.contragentID = contragentID;
        this.createTime = createTime;
        this.deleteTime = deleteTime;
    }

    @Override
    public String toString() {
        return "Cache{" +
                "CacheID=" + ID +
                ", contragentID=" + contragentID +
                ", createTime=" + createTime +
                ", deleteTime=" + deleteTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cache cache = (Cache) o;

        if (contragentID != cache.contragentID) return false;
        return contragentID == cache.contragentID;
    }

    @Override
    public int hashCode() {
        int result = (int) (contragentID ^ (contragentID >>> 32));
        result = 31 * result + (int) (contragentID ^ (contragentID >>> 32));
        return result;
    }
}
