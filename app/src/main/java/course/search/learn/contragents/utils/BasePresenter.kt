package course.search.learn.contragents.utils


interface BasePresenter {
    fun start()
}