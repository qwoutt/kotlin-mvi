package course.search.learn.contragents.utils

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.schedulers.Schedulers

class InternetConnectionIntent {
    fun observeInternetConnection() =
            ReactiveNetwork.observeInternetConnectivity()
                    .subscribeOn(Schedulers.io())


}