package course.search.learn.contragents.interactors

import course.search.learn.contragents.domain.database.AppDatabase
import course.search.learn.contragents.domain.database.dao.CacheDao
import course.search.learn.contragents.domain.database.dao.ContragentDao
import course.search.learn.contragents.domain.database.dao.FavoritDao
import course.search.learn.contragents.domain.database.entities.Cache
import course.search.learn.contragents.domain.database.entities.Contragent
import course.search.learn.contragents.search.SearchViewState
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.rxkotlin.toObservable
import io.reactivex.schedulers.Schedulers

import java.util.*
import java.util.concurrent.TimeUnit


open class DBInteractor(val db : AppDatabase) : Interactor() {

    private val favotitDao : FavoritDao = db.favoritDao()
    private val cacheDao: CacheDao = db.cacheDao()
    private val agentDao: ContragentDao = db.contragentDao()
    private val calendar = Calendar.getInstance()!!

    private val cacheAge = 1000 * 30

    private val searchByName = Function<Pair <List<Contragent>, Any>, List<Contragent>>{
        it.first.filter { contragent -> contragent.name.contains(it.second as String) }
    }

    private val searchByInn = Function<Pair <List<Contragent>, Any>, List<Contragent>>{
        it.first.filter { contragent -> contragent.inn == (it.second as Long) }
    }

    fun mergeAll(): Observable<List<Contragent>> {
        val all = favotitDao.all
        all.addAll(cacheDao.all)
        return Observable.just(all)
    }

    fun search(query: Any): Observable<SearchViewState>{
        agentDao.all.forEach { s -> print(s) }

        if( (query as String).isEmpty())
            return mergeAll()
                    .map(stateMapper(query))

        return mergeAll()
                .delay(400, TimeUnit.MILLISECONDS) // что бы показать загрузку
                .map {
                    when (query) {
                        is Long -> searchByInn.apply(Pair(it, query))
                        else -> searchByName.apply(Pair(it, query))

                    }
                    stateMapper(query.toString()).apply(it)
                }
                .startWith(SearchViewState.Loading())
                .onErrorReturn {
                    it.printStackTrace()
                    SearchViewState.Error(query.toString(), it)
                }
    }

    public fun subscribeOnAdd(observable: Observable<Contragent>){
        observable.observeOn(Schedulers.io()).subscribe(this::insertIfNew)
    }

    private fun insertIfNew(contragent: Contragent){
        var item = agentDao.getByInn(contragent.inn)
        if(item == null) {
            val contragentID = agentDao.insert(contragent)
            cacheDao.insert(Cache(contragentID,
                    calendar.timeInMillis,
                    calendar.timeInMillis + cacheAge))
        }
    }
}
