package course.search.learn.contragents

import course.search.learn.contragents.domain.AppDelegate
import course.search.learn.contragents.domain.network.DaDataApiServiсe
import course.search.learn.contragents.domain.network.Query
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before


class NetworkUnitTest {

    lateinit var api : DaDataApiServiсe

    @Before
    fun lineUP(){
        api = AppDelegate().createApiService(null)
    }

    @Test
    fun getSuggestionTest(){
        api.searchContragents(Query("Сбербанк"))
                .subscribe { next -> next.Contragents.size }

        api.searchContragents(Query("cбер"))
                .subscribe { next -> next.Contragents.size }
    }
}