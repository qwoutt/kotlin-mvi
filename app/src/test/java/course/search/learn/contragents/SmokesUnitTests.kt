package course.search.learn.contragents

import course.search.learn.contragents.domain.AppDelegate
import course.search.learn.contragents.domain.network.DaDataApiServiсe
import course.search.learn.contragents.domain.network.Query
import org.junit.Before
import org.junit.Test


class SmokesUnitTests {

    lateinit var api : DaDataApiServiсe

    @Before
    fun lineUP(){
        api = AppDelegate().createApiService(null)
    }

    fun retrofitCore(string: String){
        api.searchContragents(Query(string))
                .subscribe { next ->
                    next.Contragents.forEach { a ->
                        a.apply {
                            println(name)
                            println(manager_name)
                            println(manager_post)
                            println(inn)
                            println(kpp)
                            println(address)
                            println(if(lat == null ) 0 else lat)
                            println(if(lon == null ) 0 else lon)
                        }
                    }
                }
    }

    @Test
    fun getSuggestionTest(){
        retrofitCore("бро")
        retrofitCore("дно")
        retrofitCore("ФИТ")
        retrofitCore("ЕКН")
        retrofitCore("БАР")
        retrofitCore("СЕС")
        retrofitCore("ГЕТ")
    }

    @Test
    fun getWithoutKpp(){
        retrofitCore("caoc")
        retrofitCore("СОАО")
    }

}