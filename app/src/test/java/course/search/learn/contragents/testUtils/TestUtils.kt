package course.search.learn.contragents.testUtils

import course.search.learn.contragents.domain.database.entities.Cache
import course.search.learn.contragents.domain.database.entities.Contragent
import java.util.*

private var inc = 1L
private val baseString = "Contragent"
val random = Random()

fun rand() : Long {
    return random.nextLong()
}

public fun dataGen(): Contragent {
    val agent = Contragent()
    agent.ID = inc
    agent.name = baseString + inc++
    agent.kpp = rand()
    agent.ogrn = rand()
    agent.inn = rand()
    return agent
}

fun dataGetCache(): Cache{
    val now =  Calendar.getInstance().timeInMillis
    return Cache(dataGen().ID , now, now + 5000)
}